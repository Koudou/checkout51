# README #

Checkout 51 Coding Challenge

This is a classic symfony 5.1 application

### Author ###
Cyril Ekoule

### What is this repository for? ###

* Requirements : 
    * https://github.com/checkout51/c51-coding-challenge

### How do I get set up? ###
#### Two ways : a local server or Docker ###

##### Local server #####
* Open your console terminal, clone the repository using the command : `git clone https://Koudou@bitbucket.org/Koudou/checkout51.git`
* Move into the project directory and run composer install as follows:
    * `cd checkout51/ && composer install`
    
* Configure your database connection in the .env file line 28 for mysql @see https://symfony.com/doc/current/configuration.html#config-dot-env
    
* Install the application as follows :
    * php bin/console app:install
    
* start the local web server as follows :
    * symfony server:start

* Enjoy the application by opening your navigator and navigating to [http://127.0.0.1:8000](http://127.0.0.1:8000)

#### Docker ####
* Install Docker on your machine [docs.docker.com/compose/install](https://docs.docker.com/compose/install/)

* Open your console terminal, clone the repository using the command : `git clone https://Koudou@bitbucket.org/Koudou/checkout51.git`
* Move into the project directory as follows:
    * `cd checkout51/`
* Once you're done, run `docker-compose up -d`. This will initialise and start all the containers, then leave them running in the background
* Go into the php-fpm container (`docker-compose exec php-fpm bash`)
* Run `composer intall` 
* Install the application as follows : `php bin/console app:install` (It will create the database and insert all the offers into the database)
* Enjoy the application by opening your navigator and navigating to [http://127.0.0.1:8000](http://127.0.0.1:8000)

### Unit tests ###
* To run the application unit test, simply run `php bin/phpunit tests`
