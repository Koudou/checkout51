<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\GetOffersFromGithub;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class GetOffersFromGithubTest extends TestCase
{
    /** @var \PHPUnit\Framework\MockObject\MockObject */
    private $httpClientMock;

    /** @var \PHPUnit\Framework\MockObject\MockObject */
    private $responseMock;

    /** @var GetOffersFromGithub */
    private $getOffersFromGithub;

    protected function setUp(): void
    {
        $this->httpClientMock = $this->createMock(HttpClientInterface::class);
        $this->responseMock = $this->createMock(ResponseInterface::class);

        $this->getOffersFromGithub = new GetOffersFromGithub($this->httpClientMock, 'github_url');
    }

    public function testExecuteThenReturnOffers(): void
    {
        $offers = [
            ['name' => 'Offer 1', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 2', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 3', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 4', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 5', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 6', 'image_url' => 'image_url', 'cash_back' => 3]
        ];

        $this->responseMock->expects($this->once())->method('toArray')->willReturn(['offers' => $offers]);

        $this->httpClientMock->expects($this->once())->method('request')->willReturn(
            $this->responseMock
        );

        $this->assertEquals($offers, $this->getOffersFromGithub->execute());
    }

    public function testExecuteGivenNoExpectedResultThenReturnEmptyArray(): void
    {
        $result = [];
        $this->responseMock->expects($this->once())
            ->method('toArray')
            ->willReturnOnConsecutiveCalls(['offers' => []], null, ['batch_id' => 0]);

        $this->httpClientMock->expects($this->atLeastOnce())
            ->method('request')
            ->willReturn($this->responseMock);

        $this->assertEquals($result, $this->getOffersFromGithub->execute());
    }
}
