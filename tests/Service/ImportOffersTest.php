<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\ImportOffers;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\TestCase;

class ImportOffersTest extends TestCase
{
    /** @var \PHPUnit\Framework\MockObject\MockObject */
    private $entityManagerMock;

    /** @var \PHPUnit\Framework\MockObject\MockObject */
    private $managerRegistryMock;

    /** @var ImportOffers */
    private $importOffers;

    protected function setUp(): void
    {
        $this->entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $this->managerRegistryMock = $this->createMock(ManagerRegistry::class);

        $this->importOffers = new ImportOffers($this->entityManagerMock, $this->managerRegistryMock);
    }

    public function testExecuteGivenNoErrorThenReturnNumOfOffers(): void
    {
        $offers = [
            ['name' => 'Offer 1', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 2', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 3', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 4', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 5', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 6', 'image_url' => 'image_url', 'cash_back' => 3]
        ];

        $this->entityManagerMock->expects($this->exactly(count($offers)))->method('persist');
        $this->entityManagerMock->expects($this->atLeastOnce())->method('flush');

        $this->assertEquals(count($offers), $this->importOffers->execute($offers));
    }

    public function testExecuteGivenNullFieldsThenReturnNumOfOffersInserted(): void
    {
        $offers = [
            ['name' => null, 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 1', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 3', 'image_url' => null, 'cash_back' => 3],
            ['name' => 'Offer 3', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 5', 'image_url' => 'image_url', 'cash_back' => 3],
            ['name' => 'Offer 6', 'image_url' => 'image_url', 'cash_back' => 3]
        ];

        $nbOfValidOffers = 4;

        $this->entityManagerMock->expects($this->exactly($nbOfValidOffers))->method('persist');
        $this->entityManagerMock->expects($this->atLeastOnce())->method('flush');
        $this->managerRegistryMock->method('resetManager')->willReturn($this->entityManagerMock);

        $this->assertEquals($nbOfValidOffers, $this->importOffers->execute($offers));
    }
}
