<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Offer;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

class ImportOffers
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ManagerRegistry */
    private $managerRegistry;

    /** @var string[] */
    private $errors = [];

    /** @var string[] */
    private $warnings = [];

    public function __construct(EntityManagerInterface $entityManager, ManagerRegistry $managerRegistry)
    {
        $this->entityManager = $entityManager;
        $this->managerRegistry = $managerRegistry;
    }

    public function execute(array $offers): int
    {
        $batchSize = 20;
        $i = 1;
        try {
            foreach ($offers as $offer) {
                try {
                    $offer = (new Offer())
                        ->setName($offer['name'])
                        ->setImageUrl($offer['image_url'])
                        ->setCashBack($offer['cash_back']);

                    $this->entityManager->persist($offer);

                    if (($i % $batchSize) === 0) {
                        $this->entityManager->flush();
                    }
                    $i++;
                } catch (UniqueConstraintViolationException $exception) {
                    $this->handleDuplicateError($exception->getMessage());
                } catch (\Throwable $exception) {
                    $this->handleThrowableException($exception->getMessage());
                }
            }
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            $this->handleDuplicateError();
        } catch (\Throwable $exception) {
            $this->handleThrowableException($exception->getMessage());
        }

        return $i - 1;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getWarnings(): array
    {
        return $this->warnings;
    }

    private function reopenEntityManager(): void
    {
        if (!$this->entityManager->isOpen()) {
            $this->entityManager = $this->managerRegistry->resetManager();
        }
    }

    private function handleDuplicateError(): void
    {
        $this->warnings[] = 'Some offers you are trying to create already exist';
        $this->reopenEntityManager();
    }

    private function handleThrowableException(string $message): void
    {
        $this->errors[] = sprintf('Error while importing offer : %s', $message);
        $this->reopenEntityManager();
    }
}
