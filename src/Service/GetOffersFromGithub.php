<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class GetOffersFromGithub
{
    /** @var HttpClientInterface */
    private $httpClient;

    /** @var string */
    private $offersUrl;

    public function __construct(HttpClientInterface $httpClient, string $offersUrl)
    {
        $this->httpClient = $httpClient;
        $this->offersUrl = $offersUrl;
    }

    /**
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function execute(): array
    {
        $response = $this->httpClient->request(
            'GET',
            $this->offersUrl
        );

        return $response->toArray()['offers'] ?? [];
    }
}
