<?php

declare(strict_types=1);

namespace App\Setup\Operation;

use App\Service\GetOffersFromGithub;
use App\Service\ImportOffers;
use App\Setup\OperationInterface;

class ImportOffersOperation implements OperationInterface
{
    /** @var GetOffersFromGithub */
    private $getOffersFromGithub;

    /** @var ImportOffers */
    private $importOffers;

    /** @var array */
    private $errors = [];

    /** @var array  */
    private $warnings = [];

    /** @var int */
    private $countImportedOffers = 0;

    /**
     * ImportOffersCommand constructor.
     *
     * @param GetOffersFromGithub $getOffersFromGithub
     * @param ImportOffers $importOffers
     */
    public function __construct(GetOffersFromGithub $getOffersFromGithub, ImportOffers $importOffers)
    {
        $this->getOffersFromGithub = $getOffersFromGithub;
        $this->importOffers = $importOffers;
    }

    public function execute(): void
    {
        try {
            $this->countImportedOffers = $this->importOffers->execute(
                $this->getOffersFromGithub->execute()
            );

            $this->errors = $this->importOffers->getErrors();
            $this->warnings = $this->importOffers->getWarnings();
        } catch (\Throwable $exception) {
            $this->errors[] = $exception->getMessage();
        }
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getWarnings(): array
    {
        return $this->warnings;
    }

    public function getSuccessMessage(): string
    {
        return sprintf('You have imported %d offers', $this->countImportedOffers);
    }

    public function getSortOrder(): int
    {
        return 2;
    }
}
