<?php

declare(strict_types=1);

namespace App\Setup\Operation;

use App\Setup\OperationInterface;

/**
 * This class is not really useful. It shows the power of the dependency injection
 * by giving the possibility to add any operation easily thanks to the the symfony Tag system
 * @see /config/services.yaml:26
 */
class GoodByeOperation implements OperationInterface
{
    public function execute(): void
    {
    }

    public function getErrors(): array
    {
        return [];
    }

    public function getWarnings(): array
    {
        return [];
    }

    public function getSuccessMessage(): string
    {
        return 'Enjoy the offers. I hope to see you soon.';
    }

    public function getSortOrder(): int
    {
        return 10;
    }
}
