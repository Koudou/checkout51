<?php

declare(strict_types=1);

namespace App\Setup\Operation;

use App\Setup\OperationInterface;
use Doctrine\Bundle\DoctrineBundle\Command\CreateDatabaseDoctrineCommand;
use Doctrine\Migrations\Tools\Console\Command\MigrateCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateDBOperation implements OperationInterface
{
    /** @var CreateDatabaseDoctrineCommand */
    private $createDatabaseDoctrineCommand;

    /** @var MigrateCommand */
    private $migrateCommand;

    public function __construct(
        CreateDatabaseDoctrineCommand $createDatabaseDoctrineCommand,
        MigrateCommand $migrateCommand
    ) {
        $this->createDatabaseDoctrineCommand = $createDatabaseDoctrineCommand;
        $this->migrateCommand = $migrateCommand;
    }

    public function execute(): void
    {
        $output = new ConsoleOutput();

        $inputCreate = new ArrayInput(['--if-not-exists' => true]);
        $inputCreate->setInteractive(false);

        $this->createDatabaseDoctrineCommand->run($inputCreate, $output);

        $inputMigrate = new ArrayInput([]);
        $inputMigrate->setInteractive(false);

        $this->migrateCommand->run($inputMigrate, $output);
    }

    public function getErrors(): array
    {
        return [];
    }

    public function getWarnings(): array
    {
        return [];
    }

    public function getSuccessMessage(): string
    {
        return 'The Database was created.';
    }

    public function getSortOrder(): int
    {
        return 1;
    }
}
