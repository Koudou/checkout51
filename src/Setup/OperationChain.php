<?php

declare(strict_types=1);

namespace App\Setup;

class OperationChain
{
    /** @var OperationInterface[] */
    private $operations = [];

    /**
     * @return OperationInterface[]
     */
    public function getOperations(): array
    {
        usort($this->operations, function (OperationInterface $operation1, OperationInterface $operation2) {
            if ($operation1->getSortOrder() === $operation2->getSortOrder()) {
                return 0;
            }

            return ($operation1->getSortOrder() < $operation2->getSortOrder()) ? -1 : 1;
        });

        return $this->operations;
    }

    /**
     * Not typing $operation parameter because
     * we don't want to throw an error if it has the wrong type
     *
     * @param OperationInterface $operation
     * @return $this
     */
    public function addOperation($operation): self
    {
        if ($operation instanceof OperationInterface) {
            $this->operations[] = $operation;
        }

        return $this;
    }
}

