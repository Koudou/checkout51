<?php

declare(strict_types=1);

namespace App\Setup;

interface OperationInterface
{
    public function execute(): void;

    public function getErrors(): array;

    public function getWarnings(): array;

    public function getSuccessMessage(): string;

    public function getSortOrder(): int;
}
