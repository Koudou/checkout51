<?php

declare(strict_types=1);

namespace App\DependencyInjection\Compiler;

use App\Setup\OperationChain;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class OperationPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(OperationChain::class)) {
            return;
        }

        $definition = $container->findDefinition(OperationChain::class);
        $taggedServices = $container->findTaggedServiceIds('app.operation');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addOperation', [new Reference($id)]);
        }
    }
}
