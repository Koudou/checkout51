<?php

namespace App\Command;

use App\Setup\OperationChain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class InstallCommand extends Command
{
    protected static $defaultName = 'app:install';

    /** @var OperationChain */
    private $operationChain;

    public function __construct(OperationChain $operationChain)
    {
        parent::__construct();
        $this->operationChain = $operationChain;
    }

    protected function configure()
    {
        $this->setDescription(
            'Install application : Database creation, offers import.
             It uses the Chain of Responsibility design pattern.
             To add a new Operation, simply create a class into src/Setup/Operation. 
             This class must implement \App\Setup\OperationInterface.'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $hasError = false;

        foreach ($this->operationChain->getOperations() as $operation) {
            $operation->execute();

            if (count($operation->getErrors()) > 0) {
                $io->error($operation->getErrors());
                $hasError = true;
            }
            if (count($operation->getWarnings()) > 0) {
                $io->warning($operation->getWarnings());
            }

            if (!$hasError) {
                $io->success($operation->getSuccessMessage());
            }
        }

        $io->success('The application is now installed.');

        return Command::SUCCESS;
    }
}
