<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OfferController extends AbstractController
{
    /** @var OfferRepository */
    private $offerRepository;

    public function __construct(OfferRepository $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }

    /**
     * @Route("/", name="offers")
     */
    public function index(Request $request): Response
    {
        $sortBy = $request->get('sort_by');
        $direction = $request->get('direction', 'ASC');

        $orderBy = $sortBy ? [$sortBy => $direction] : [];

        return $this->render('offer/index.html.twig', [
            'offers' => $this->offerRepository->findBy([], $orderBy),
            'current_sort_direction' => $direction,
            'current_sort_by' => $sortBy
        ]);
    }
}
